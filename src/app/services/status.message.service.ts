import {Injectable} from "@angular/core";
import {environment} from "../../environments/environment";
import {HttpClient, HttpErrorResponse} from "@angular/common/http";

@Injectable({providedIn:"root"})
export class StatusMessageService {

  statusMsg: string = "";

  constructor(
    private http: HttpClient
  ) {
  }

  on200() {
    this.statusMsg = "";
    this.http.get(environment.serverUrl + "/get200").subscribe(resp => {
      this.statusMsg = "Response: OK";
    });
  }

  on401() {
    this.statusMsg = "";
    this.http.get(environment.serverUrl + "/get401").subscribe((resp: HttpErrorResponse) => {
    });
  }

  on403() {
    this.statusMsg = "";
    this.http.get(environment.serverUrl + "/get403").subscribe((resp: HttpErrorResponse) => {
    });
  }

  on500() {
    this.statusMsg = "";
    this.http.get(environment.serverUrl + "/get500").subscribe((resp: HttpErrorResponse) => {
      this.statusMsg = resp.message;
    });
  }
}
