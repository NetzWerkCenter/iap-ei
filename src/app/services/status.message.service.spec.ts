import {environment} from "../../environments/environment";
import {TestBed} from "@angular/core/testing";
import {HTTP_INTERCEPTORS} from "@angular/common/http";
import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";

import {IapExceptionInterceptorFeLibModule} from "../../../projects/iap-exception-interceptor-fe-lib/src/lib/iap-exception-interceptor-fe-lib.module";
import {HttpInterceptorWorkerImpl} from "../interceptor/http.interceptor.worker.impl";
import {StatusMessageService} from "./status.message.service";
import {IapExceptionInterceptorFeLibService} from "../../../projects/iap-exception-interceptor-fe-lib/src/lib/iap-exception-interceptor-fe-lib.service";

describe('StatusMessageService', () => {

  let statusMessageService: StatusMessageService;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        StatusMessageService,
        {
          provide: HTTP_INTERCEPTORS,
          useClass: IapExceptionInterceptorFeLibService,
          multi: true
        },
      ],
      imports: [
        IapExceptionInterceptorFeLibModule.forRoot(HttpInterceptorWorkerImpl),
        HttpClientTestingModule
      ]
    });

    statusMessageService = TestBed.get(StatusMessageService);
    httpTestingController = TestBed.get(HttpTestingController)
  });

  xit("can handle http status 200", () => {
    statusMessageService.on200();
    const req = httpTestingController.expectOne(environment.serverUrl + "/get200");
    expect(req.request.method).toEqual("GET");
    req.flush("");
    expect(statusMessageService.statusMsg).toEqual("Response: OK");
  });

  xit("can handle http status 401", () => {
    statusMessageService.on401();
    const req = httpTestingController.expectOne(environment.serverUrl + "/get401");
    expect(req.request.method).toEqual("GET");
    req.flush("You are not authorized", {status: 401, statusText: ''});
    expect(statusMessageService.statusMsg).toEqual("<b>Unauthorized Request:</b><br><br>Body: You are not authorized<br><br>Status: 401<br><br>");
  });

  xit("can handle http status 403", () => {
    statusMessageService.on403();
    const req = httpTestingController.expectOne(environment.serverUrl + "/get403");
    expect(req.request.method).toEqual("GET");
    req.flush("Forbidden", {status: 403, statusText: ''});
    expect(statusMessageService.statusMsg).toEqual("<b>Forbidden Request:</b><br><br>Body: Forbidden<br><br>Status: 403<br><br>");
  });

  xit("can handle http status 500", () => {
    statusMessageService.on500();
    const req = httpTestingController.expectOne(environment.serverUrl + "/get500");
    expect(req.request.method).toEqual("GET");
    req.flush({"message": "001", "timestamp": "2017-10-30T16:06:05Z", "stacktrace": "002"}, {status: 500, statusText: ""});
    expect(statusMessageService.statusMsg).toEqual("<b>Internal Server Error:</b><br><br>Status: 500<br><br>Message: 001<br><br>Timestamp: 2017-10-30T16:06:05Z<br><br>StackTrace:<br>002<br><br>");
  });

  afterEach(() => {
    httpTestingController.verify();
    TestBed.resetTestingModule();
  })
});
