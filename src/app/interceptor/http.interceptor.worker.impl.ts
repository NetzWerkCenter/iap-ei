import {Injectable} from "@angular/core";
import {HttpInterceptorWorker} from "../../../projects/iap-exception-interceptor-fe-lib/src/lib/httpInterceptorWorker";
import {StatusMessageService} from "../services/status.message.service";
import {HttpErrorResponse} from "@angular/common/http";
import {InternalServerErrorImpl} from "../../../projects/iap-exception-interceptor-fe-lib/src/lib/model/internal.server.error.impl";

@Injectable({providedIn: "root"})
export abstract class HttpInterceptorWorkerImpl implements HttpInterceptorWorker {

  constructor(
    private statusMessageService: StatusMessageService
  ) {
  }

  workWith401(error: HttpErrorResponse): void {
    let message = "<b>Unauthorized Request:</b><br><br>" +
      "Body: " + error.error + "<br><br>" +
      "Status: " + error.status + "<br><br>";
    this.statusMessageService.statusMsg = message;
  }

  workWith403(error: HttpErrorResponse): void {
    let message = "<b>Forbidden Request:</b><br><br>" +
      "Body: " + error.error + "<br><br>" +
      "Status: " + error.status + "<br><br>";
    this.statusMessageService.statusMsg = message;
  }

  workWith500(error: HttpErrorResponse, ise: InternalServerErrorImpl): void {
    let message = "<b>Internal Server Error:</b><br><br>" +
      "Status: " + error.status + "<br><br>" +
      "Message: " + ise.message + "<br><br>" +
      "Timestamp: " + ise.timestamp + "<br><br>" +
      "StackTrace:<br>" + ise.stacktrace + "<br><br>";
    this.statusMessageService.statusMsg = message;
  }

}
