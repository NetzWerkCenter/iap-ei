import {async, TestBed} from '@angular/core/testing';
import {AppComponent} from './app.component';
import {IapExceptionInterceptorFeLibService} from "../../projects/iap-exception-interceptor-fe-lib/src/lib/iap-exception-interceptor-fe-lib.service";
import {IapExceptionInterceptorFeLibModule} from "../../projects/iap-exception-interceptor-fe-lib/src/lib/iap-exception-interceptor-fe-lib.module";
import {HttpInterceptorWorkerImpl} from "./interceptor/http.interceptor.worker.impl";
import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
import {HTTP_INTERCEPTORS} from "@angular/common/http";
import {BrowserModule} from "@angular/platform-browser";

describe('AppComponent', () => {

  let fixture, app, httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent
      ],
      providers: [
        {
          provide: HTTP_INTERCEPTORS,
          useClass: IapExceptionInterceptorFeLibService,
          multi: true
        }
      ],
      imports: [
        BrowserModule,
        IapExceptionInterceptorFeLibModule.forRoot(HttpInterceptorWorkerImpl),
        HttpClientTestingModule
      ]
    }).compileComponents();


    fixture = TestBed.createComponent(AppComponent);
    app = fixture.debugElement.componentInstance;
    httpTestingController = TestBed.get(HttpTestingController);
    fixture.detectChanges();
  });

  it('should create the app', () => {
    expect(app).toBeTruthy();
  });

  it(`should have as title 'iap-exception-interceptor-fe'`, () => {
    expect(app.title).toEqual('iap-exception-interceptor-fe');
  });

  afterEach(() => {
    httpTestingController.verify();
    TestBed.resetTestingModule();
  })
});
