import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {IapExceptionInterceptorFeLibModule} from "../../projects/iap-exception-interceptor-fe-lib/src/lib/iap-exception-interceptor-fe-lib.module";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {IapExceptionInterceptorFeLibService} from "../../projects/iap-exception-interceptor-fe-lib/src/lib/iap-exception-interceptor-fe-lib.service";
import {HttpInterceptorWorkerImpl} from "./interceptor/http.interceptor.worker.impl";

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    IapExceptionInterceptorFeLibModule.forRoot(HttpInterceptorWorkerImpl),
    HttpClientModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: IapExceptionInterceptorFeLibService,
      multi: true
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
