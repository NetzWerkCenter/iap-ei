import {Component} from '@angular/core';
import {StatusMessageService} from "./services/status.message.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {

  title = 'iap-exception-interceptor-fe';
  showDoku: boolean = false;

  constructor(
    public statusMessageService: StatusMessageService
  ) {
  }
}
