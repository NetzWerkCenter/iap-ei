import {HttpErrorResponse} from "@angular/common/http";
import {InternalServerErrorImpl} from "./model/internal.server.error.impl";

export interface HttpInterceptorWorker {
  workWith401(error: HttpErrorResponse): void;
  workWith403(error: HttpErrorResponse): void;
  workWith500(error: HttpErrorResponse, internalServerError: InternalServerErrorImpl): void;
}
