import {Inject, Injectable} from '@angular/core';
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpRequest, HttpResponse} from "@angular/common/http";
import {map, tap} from "rxjs/operators";
import {Observable} from "rxjs";
import {HttpInterceptorWorker} from "./httpInterceptorWorker";
import {InternalServerErrorImpl} from "./model/internal.server.error.impl";

@Injectable({providedIn: "root"})
export class IapExceptionInterceptorFeLibService {

  constructor(
    @Inject('HttpInterceptorWorker') protected httpInterceptorWorker: HttpInterceptorWorker
  ) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(tap(() => {
      },
      (err: any) => {
        if (err instanceof HttpErrorResponse) {
          if (err.status === 200) {
            return;
          }
          switch (err.status) {
            case 401:
              this.httpInterceptorWorker.workWith401(err);
              break;
            case 403:
              this.httpInterceptorWorker.workWith403(err);
              break;
            case 500:
              let ise:InternalServerErrorImpl = Object.assign(new InternalServerErrorImpl(), err.error);
              this.httpInterceptorWorker.workWith500(err, ise);
              break;
          }
        }
      }));
  }

}
