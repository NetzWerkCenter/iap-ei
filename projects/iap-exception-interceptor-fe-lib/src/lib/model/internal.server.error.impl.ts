import { InternalServerError } from '../../generated-sources/model/internalServerError';

export class InternalServerErrorImpl implements InternalServerError{
  message: string;
  stacktrace?: string;
  timestamp: Date;
}
