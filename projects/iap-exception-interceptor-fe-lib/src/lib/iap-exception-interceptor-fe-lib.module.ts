import {ModuleWithProviders, NgModule} from '@angular/core';
import {HttpInterceptorWorker} from "./httpInterceptorWorker";


@NgModule({
  declarations: [],
  imports: [],
  exports: [],
  providers: []
})
export class IapExceptionInterceptorFeLibModule {
  static forRoot(httpInterceptorWorker): ModuleWithProviders {
    return {
      ngModule: IapExceptionInterceptorFeLibModule,
      providers: [
        {provide: 'HttpInterceptorWorker', useClass: httpInterceptorWorker}
      ]
    };
  }
}
